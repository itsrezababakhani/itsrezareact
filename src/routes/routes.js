import Home from "../containers/Home";
import portfolio from "../containers/Portfolio";
import Courses from "../containers/Courses";
import Resume from "../containers/Resume";
import UserLogout from "../components/user/UserLogout";
import PortfolioPage from "../containers/PortfolioPage";
import CoursePage from "../containers/CoursePage";
const routes = [
  {
    path: "/",
    component: Home,
    exact: true
  },
  {
    path: "/portfolio",
    component: portfolio,
    exact: true
  },
  {
    path: "/courses",
    component: Courses,
    exact: true
  },
  {
    path: "/user/logout",
    component: UserLogout,
    exact: true
  },
  {
    path: "/portfolio/:portfolioID",
    component: PortfolioPage,
    exact: true
  },
  {
    path: "/course/:courseID",
    component: CoursePage,
    exact: true
  },
  { path: "/resume", component: Resume, exact: true }
];
export default routes;
