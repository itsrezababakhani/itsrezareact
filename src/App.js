import React from "react";
import NotFoundError from "./components/errors/NotFoundError";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import routes from "./routes/routes";

function App() {
  const router = routes.map((route, index) => {
    return (
      <Route
        key={index}
        component={route.component}
        path={route.path}
        exact={route.exact}
      />
    );
  });
  return (
    <Router>
      <Switch>
        {router}
        <Route component={NotFoundError} />
      </Switch>
    </Router>
  );
}

export default App;
