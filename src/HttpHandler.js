import axios from "axios";
import { Component } from "react";
export default class HttpHandler extends Component {
  constructor(props) {
    super(props);
    this.preUrl = "http://localhost:5000";
  }
  get(url) {
    return axios.get(`${this.preUrl}${url}`);
  }
  post(url, params) {
    return axios.post(`${this.preUrl}${url}`, params);
  }
}
