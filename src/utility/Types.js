const types = {
  "1": "article",
  "2": "file",
  "3": "video"
};
export default function Types(input) {
  return types[input];
}
