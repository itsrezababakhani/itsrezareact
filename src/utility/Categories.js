const categories = {
  "1": "reactjs",
  "2": "nodejs",
  "3": "illustrator",
  "4": "adobexd",
  "5": "photoshop"
};
export default function Categories(input) {
  return categories[input];
}
