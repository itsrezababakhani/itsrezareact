const category = {
  "1": "رابط کاربری",
  "2": "ریکت",
  "3": "ریکت نیتیو",
  "4": "نود جی اس",
  "5": "تجربه ی کاربری",
  "6": "برنامه نویسی"
};
export default function CategoryByName(input) {
  return category[input];
}
