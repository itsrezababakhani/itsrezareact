import React, { useState, useEffect } from "react";
import HttpHandler from "../HttpHandler";
import FullHeader from "../components/headers/FullHeader";
import TitleIndex from "../components/partials/TitleIndex";
export default function PortfolioPage({ match }) {
  const http = new HttpHandler();
  const [data, setData] = useState([]);
  useEffect(() => {
    http
      .post("/api/v1/portfolio/uid", { id: match.params.portfolioID })
      .then(result => {
        return setData(result.data.data);
      })
      .catch(err => err);
  }, [data.length]);
  const renderData = data.map(post => {
    return (
      <div className="container">
        <TitleIndex fa_title={post.title} en_title={post.eng_title} />
        <hr />
        <div className="row">
          <div className="col-lg-3 meta_info float-right">
            <span className="title_meta">اطلاعات </span>
            <ul className="info_list">
              <li>
                <span className="float-right">سازنده :</span>
                <span className="float-left">{post.creator}</span>
                <div className="clearfix"></div>
              </li>
              <li>
                <span className="float-right">سال تولید :</span>
                <span className="float-left">{post.year}</span>
                <div className="clearfix"></div>
              </li>
            </ul>
          </div>
          <div className="col-lg-9 float-left">{post.description}</div>
          <div className="clear"></div>
        </div>
      </div>
    );
  });
  return (
    <div>
      <FullHeader />
      {renderData}
    </div>
  );
}
