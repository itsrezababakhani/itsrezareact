import React, { useState } from "react";
import TitleIndex from "../components/partials/TitleIndex";
import AddressBar from "../components/partials/AddressBar";
import PortfolioShow from "../components/Portfolio/PortfolioShow";
import PortfolioFilter from "../components/Portfolio/PortfolioFilter";
import FullHeader from "../components/headers/FullHeader";
export default function Portfolio() {
  return (
    <div>
      <FullHeader />
      <TitleIndex fa_title="نمونه کار های من" en_title="PORTFOLIO" />
      <div className="container">
        <AddressBar class="ads_section" title="نمونه کار ها" />
        <hr class="sub_line" />
        <div className="row">
          <div className="col-lg-9">
            <PortfolioShow />
          </div>
          <div className="col-lg-3">
            <PortfolioFilter />
          </div>
        </div>
      </div>
    </div>
  );
}
