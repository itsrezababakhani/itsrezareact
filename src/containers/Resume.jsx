import React from "react";
import FullHeader from "../components/headers/FullHeader";
import ResumeHeader from "../components/resume/ResumeHeader";
import ResumeTitle from "../components/resume/ResumeTitle";
import AboutWrapper from "../components/resume/AboutWrapper";
import TechnicalWrapper from "../components/resume/TechnicalWrapper";
import SocialWrapper from "../components/resume/SocialWrapper";
export default function Resume() {
  return (
    <div>
      <FullHeader />
      <div className="resume_section">
        <ResumeHeader />
        <div className="container resume_content">
          <ResumeTitle title="About Me" />
          <AboutWrapper />
          <ResumeTitle title="Technical" />
          <TechnicalWrapper />
          <ResumeTitle title="Social Networks" />
          <SocialWrapper />
        </div>
      </div>
    </div>
  );
}
