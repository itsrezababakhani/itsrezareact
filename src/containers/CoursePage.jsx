import React, { useEffect, useState } from "react";
import HttpHandler from "../HttpHandler";
import TitleIndex from "../components/partials/TitleIndex";
import FullHeader from "../components/headers/FullHeader";

export default function CoursePage({ match }) {
  const http = new HttpHandler();

  const [data, setData] = useState([]);

  const courseID = match.params.courseID;
  useEffect(() => {
    http
      .post("/api/v1/tutorial/id", { id: courseID })
      .then(result => {
        return setData(result.data.data);
      })
      .catch(err => err)
      .finally(() => {
        // setLoading(false);
      });
  }, data.length);

  const renderItem = data.map(result => {
    return (
      <div>
        <div className="container">
          <TitleIndex fa_title={result.title} en_title={result.eng_title} />

          <hr />
          <div className="row">
            <div className="col-lg-3 meta_info float-right">
              <span className="title_meta">اطلاعات </span>
              <ul className="info_list">
                <li>
                  <span className="float-right">نویسنده :</span>
                  <span className="float-left">{result.author}</span>
                  <div className="clearfix"></div>
                </li>
                <li>
                  <span className="float-right">سال تولید :</span>
                  <span className="float-left">{result.created_at}</span>
                  <div className="clearfix"></div>
                </li>
              </ul>
            </div>
            <div className="col-lg-9 float-left text_result">
              {result.description}
            </div>
            <div className="clear"></div>
          </div>
        </div>
      </div>
    );
  });

  return (
    <div>
      <FullHeader />

      {renderItem}
    </div>
  );
}
