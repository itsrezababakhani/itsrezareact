import React, { useState } from "react";
import { Redirect } from "react-router-dom";
import Menu from "../components/headers/Menu";
import SideMenu from "../components/headers/SideMenu";
import RegisterModal from "../components/headers/RegisterModal";
import HttpHandler from "../HttpHandler";
import swal from "sweetalert";
import LoginModal from "../components/headers/LoginModal";
import Introduce from "../components/index/Introduce";
import TitleIndex from "../components/partials/TitleIndex";
import PortfolioMin from "../components/index/PortfolioMin";
import Spliter from "../components/partials/Spliter";
import TrendsArticles from "../components/index/TrendsArticles";
import CourseMin from "../components/index/CourseMin";
import Projection from "../components/index/Projection";
// import Loading from "../components/loading/Loading.jsx";
// import BrandsSlider from "../components/index/BrandsSlider";
function Home() {
  const http = new HttpHandler();
  let [showModal, setModal] = useState(false);
  let [showLogin, setLoginShow] = useState(false);
  const modalHandler = e => {
    setModal(true);
  };
  const closeModal = e => {
    setModal(false);
  };

  const loginModalHandler = e => {
    setLoginShow(true);
  };
  const closeLogin = e => {
    setLoginShow(false);
  };

  const signInHandler = (e, userData) => {
    http
      .post("/api/v1/auth/login", userData)
      .then(result => {
        if (result.data.success) {
          swal({
            title: result.data.message,
            text: "خوش آمدید .. :)",
            icon: "success",
            button: "ورود به پنل کاربری"
          }).then(res => {
            localStorage.setItem("token", result.data.token); /// token wrong set in session pure
            {
              document.location.reload(true);
            }
          });
        }
      })
      .catch(error => {
        if (error.response.status != 200) {
          swal({
            title: "خطایی رخ داده است",
            text: error.response.data.message,
            icon: "warning",
            button: "تکمیل اطلاعات"
          });
        }
      });
  };

  const signUpHandler = (e, userData) => {
    http
      .post("/api/v1/auth/register", userData)
      .then(result => {
        if (result.data.success) {
          swal({
            title: result.data.message,
            text: "خوش آمدید .. :)",
            icon: "success",
            button: "ورود به پنل کاربری"
          }).then(res => {
            localStorage.setItem("token", result.data.token); /// token wrong set in session pure
            {
              document.location.reload(true);
            }
          });
        } else {
          swal({
            title: "خطا در ثبت نام",
            text: result.data.message,
            icon: "error",
            button: "بستن"
          });
        }
      })
      .catch(error => {
        if (error.response.status != 200) {
          swal({
            title: "خطا در ثبت نام",
            text: error.response.data.message,
            icon: "error",
            button: "تکمیل اطلاعات"
          });
        }
      });
  };

  return (
    <div>
      {showLogin && (
        <LoginModal
          showLogin={showLogin}
          onClose={closeLogin}
          onSignIn={signInHandler}
        />
      )}

      {showModal && (
        <RegisterModal
          show={showModal}
          onClose={closeModal}
          onSign={signUpHandler}
        />
      )}

      <div className="container-fluid main_container">
        <div className="row section_navs">
          <Menu />

          <SideMenu
            modal={modalHandler}
            modalLoginHandler={loginModalHandler}
          />
          <div className="clearfix"></div>
        </div>
        <Introduce />
        <div className="mouse_scroll"></div>
      </div>
      <TitleIndex fa_title="نمونه کار های من" en_title="PORTFOLIO" />
      <PortfolioMin />
      <div className="container portfolio_container">
        <Spliter title="پربازدید ترین مطالب" />
        <hr className="sub_line" />
        <TrendsArticles />
      </div>
      <div className="container portfolio_container">
        <Spliter title="آموزش ها" />
        <hr className="sub_line" />
        <CourseMin />
        <Projection />
      </div>
      {/* <TitleIndex fa_title="متفکران برتر" en_title="BRANDS" />
      <div className="container-fluid">
        <BrandsSlider />
      </div> */}
    </div>
  );
}
export default Home;
