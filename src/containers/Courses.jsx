import React, { useState } from "react";

import TitleIndex from "../components/partials/TitleIndex";
import AddressBar from "../components/partials/AddressBar";
import CourseMin from "../components/index/CourseMin";
import FullHeader from "../components/headers/FullHeader";

export default function Courses() {
  return (
    <div>
      <FullHeader />

      <TitleIndex fa_title="آموزش ها" en_title="tutorials" />
      <div className="container">
        <AddressBar class="ads_section" title="آموزش ها" />
        <hr class="sub_line" />
        <div className="row">
          <div className="col-lg-12">
            <CourseMin />
          </div>
        </div>
      </div>
    </div>
  );
}
