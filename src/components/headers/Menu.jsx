import React from "react";
import { NavLink } from "react-router-dom";
export default function Header() {
  return (
    <div className="main_navigator col float-right">
      <ul className="nav">
        <li className="nav-item">
          <NavLink className="nav-link hvr-grow" to="/">
            صفحه ی اصلی
          </NavLink>
        </li>
        <li className="nav-item">
          <NavLink className="nav-link hvr-grow" to="/portfolio">
            نمونه کار ها
          </NavLink>
        </li>
        <li className="nav-item">
          <NavLink className="nav-link hvr-grow" to="/courses">
            آموزش ها
          </NavLink>
        </li>
        <li className="nav-item">
          <NavLink className="nav-link hvr-grow" to="/resume">
            رزومه
          </NavLink>
        </li>
      </ul>
    </div>
  );
}
