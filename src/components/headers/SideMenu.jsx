import React from "react";
import UserLoginMenu from "./UserLoginMenu";

export default function SideMenu(props) {
  return (
    <div className="float-left">
      <ul className="nav category_nav">
        <li className="nav-item">
          <a className="category_icon nav-link hvr-grow" href=" #">
            <img src="../assets/images/photoshop.svg" alt="photoshop" />
          </a>
        </li>
        <li className="nav-item">
          <a className="category_icon nav-link hvr-grow" href=" #">
            <img src="../assets/images/adobexd.svg" alt="adobexd" />
          </a>
        </li>
        <li className="nav-item">
          <a className="category_icon nav-link hvr-grow" href=" #">
            <img src="../assets/images/illustrator.svg" alt="illustrator" />
          </a>
        </li>
        <li className="nav-item">
          <a className="category_icon nav-link hvr-grow" href=" #">
            <img src="../assets/images/nodejs.svg" alt="nodejs" />
          </a>
        </li>
        <li className="nav-item">
          <a className="category_icon nav-link hvr-grow" href=" #">
            <img src="../assets/images/reactjs.svg" alt="reactjs" />
          </a>
        </li>

        {localStorage.token ? (
          <UserLoginMenu />
        ) : (
          <div className="d-flex align-items-center ">
            <li className="nav-item d-flex align-items-center">
              <span
                onClick={e => {
                  props.modalLoginHandler(e);
                }}
                className="login_button"
              >
                ورود
              </span>
            </li>
            <li className="nav-item d-flex align-items-center">
              <span
                onClick={e => {
                  props.modal(e);
                }}
                className="register_button"
              >
                عضویت
              </span>
            </li>
          </div>
        )}
      </ul>
    </div>
  );
}
