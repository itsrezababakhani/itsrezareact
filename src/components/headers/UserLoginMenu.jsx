import React from "react";
import { NavLink } from "react-router-dom";
export default function UserLoginMenu() {
  return (
    <div className="loged_in_nav nav-item d-flex align-items-center">
      <span className="nav-item d-flex align-items-center">
        <img
          className="nav-item d-flex align-items-center default_ava"
          src="assets/images/default_avatar.svg"
          alt="defaultAvatar"
        ></img>
        {localStorage.username}{" "}
      </span>

      <div class="dropdown-menu drop_user_menu">
        <NavLink className="dropdown-item">پروفایل</NavLink>

        <NavLink to="/user/logout" className="dropdown-item">
          خروج
        </NavLink>
      </div>
    </div>
  );
}
