import React, { useState } from "react";
import Menu from "./Menu";
import SideMenu from "./SideMenu";
import RegisterModal from "./RegisterModal";
import HttpHandler from "../../HttpHandler";
import swal from "sweetalert";
import LoginModal from "./LoginModal";
export default function FullHeader() {
  const http = new HttpHandler();
  let [showModal, setModal] = useState(false);
  let [showLogin, setLoginShow] = useState(false);
  const modalHandler = e => {
    setModal(true);
  };
  const closeModal = e => {
    setModal(false);
  };

  const loginModalHandler = e => {
    setLoginShow(true);
  };
  const closeLogin = e => {
    setLoginShow(false);
  };

  const signInHandler = (e, userData) => {
    http
      .post("/api/v1/auth/login", userData)
      .then(result => {
        if (result.data.success) {
          localStorage.setItem("token", result.data.token); /// token wrong set in session pure
          localStorage.setItem("username", result.data.username);
          swal({
            title: result.data.message,
            text: "خوش آمدید .. :)",
            icon: "success",
            button: "ورود به پنل کاربری"
          });
        }
      })
      .catch(error => {
        if (error.response.status != 200) {
          swal({
            title: "خطایی رخ داده است",
            text: error.response.data.message,
            icon: "warning",
            button: "تکمیل اطلاعات"
          });
        }
      });
  };

  const signUpHandler = (e, userData) => {
    http
      .post("/api/v1/auth/register", userData)
      .then(result => {
        if (result.data.success) {
          swal({
            title: result.data.message,
            text: "خوش آمدید .. :)",
            icon: "success",
            button: "ورود به پنل کاربری"
          });
          localStorage.setItem("token", result.data.token); /// token wrong set in session pure
          localStorage.setItem("username", userData.phone);
        } else {
          swal({
            title: "خطا در ثبت نام",
            text: result.data.message,
            icon: "error",
            button: "بستن"
          });
        }
      })
      .catch(error => {
        if (error.response.status != 200) {
          swal({
            title: "خطا در ثبت نام",
            text: error.response.data.message,
            icon: "error",
            button: "تکمیل اطلاعات"
          });
        }
      });
  };
  return (
    <div>
      <LoginModal
        showLogin={showLogin}
        onClose={closeLogin}
        onSignIn={signInHandler}
      />
      <RegisterModal
        show={showModal}
        onClose={closeModal}
        onSign={signUpHandler}
      />
      <div className="container-fluid">
        <div className="row section_navs">
          <Menu />
          <SideMenu
            modal={modalHandler}
            modalLoginHandler={loginModalHandler}
          />
          <div className="clearfix"></div>
        </div>
      </div>
    </div>
  );
}
