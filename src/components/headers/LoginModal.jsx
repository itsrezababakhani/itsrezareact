import React, { useState } from "react";
import * as EmailValidator from "email-validator";
import swal from "sweetalert";

export default function LoginModal(props) {
  const userSignIn = e => {
    const userData = {
      email: email,
      password: password
    };
    props.onSignIn(e, userData);
  };

  const viewPasswordHandler = e => {
    if (viewPassword === "password") {
      setViewPassword("text");
    } else {
      setViewPassword("password");
    }
  };

  let [email, setEmail] = useState("");
  let [password, setPassword] = useState("");
  let [viewPassword, setViewPassword] = useState("password");

  const changeHandler = e => {
    const changeInput = e.target.name;
    if (changeInput === "email") {
      setEmail(e.target.value);
    } else {
      setPassword(e.target.value);
    }
  };

  return (
    <div
      className={`modal_overlay modal-${
        props.showLogin ? "LoginShow" : "LoginHide"
      }`}
    >
      <div className="modal-contents">
        <div className="modal-header">
          <img
            onClick={e => {
              props.onClose(e);
            }}
            className="close_modal"
            src="assets/images/cancel.svg"
            alt="close"
          />

          {/* <h4 className="register_title">ورود</h4> */}
        </div>
        <div className="modal_bodies">
          <div className="clearfix"></div>
          <form className="register_form" id="register_form">
            <div className="box-input">
              <label className="registerEmail " htmlFor="registerEmail">
                ایمیل :
              </label>

              {EmailValidator.validate(email) ? (
                <span className="ios_emoji ec ec-blush"></span>
              ) : (
                <span className="ios_emoji ec ec-cold-sweat"></span>
              )}
              <input
                type="email"
                className="form-control"
                id="registerEmail"
                name="email"
                value={email}
                placeholder="ایمیل خود را وارد نمایید ..."
                onChange={e => {
                  changeHandler(e);
                }}
              />
            </div>
            <div className="box-input box-password">
              <label className="registerEmail " htmlFor="registerPassword">
                کلمه ی عبور :
              </label>
              {password.length < 4 && password.length > 1 ? (
                <span className="ios_emoji ec ec-slightly-frowning-face"></span>
              ) : null}
              {password.length >= 4 && password.length < 6 ? (
                <span className="ios_emoji ec ec-cold-sweat"></span>
              ) : null}
              {password.length >= 6 ? (
                <span className="ios_emoji ec ec-blush"></span>
              ) : null}
              <input
                type={viewPassword}
                className="form-control"
                id="registerPassword"
                name="password"
                value={password}
                onChange={e => {
                  changeHandler(e);
                }}
                placeholder="کلمه ی عبور خود را وارد نمایید ..."
              />
              <span
                onClick={e => {
                  viewPasswordHandler(e);
                }}
                className="view_password"
              >
                <i className="material-icons">visibility</i>
              </span>
            </div>
            <button
              onClick={e => {
                e.preventDefault();
                if (EmailValidator.validate(email) && password.length >= 6) {
                  userSignIn(e);
                } else {
                  swal({
                    title: "اطلاعات ناقص است",
                    text: "لطفا مشخصات را به درستی پر کنید",
                    icon: "error",
                    button: "تغییر اطلاعات"
                  });
                }
              }}
              className="button_signup"
            >
              ورود
            </button>
          </form>
        </div>
      </div>
    </div>
  );
}
