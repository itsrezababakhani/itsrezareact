import React from "react";
export default function ProgressBar({ title, percent, value }) {
  return (
    <div className="skill_box">
      {title}
      <div className="progress">
        <div
          className="progress-bar green_bar"
          role="progressbar"
          style={{ width: percent }}
          aria-valuenow={value}
          aria-valuemin="0"
          aria-valuemax={value}
        >
          {percent}
        </div>
      </div>
    </div>
  );
}
