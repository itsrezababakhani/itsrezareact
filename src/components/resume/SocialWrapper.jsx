import React from "react";
import { Redirect, NavLink } from "react-router-dom";
export default function SocialWrapper() {
  return (
    <div class="wrapper">
      Hire Me : +989309050997 <br />
      Linkedin :{" "}
      <NavLink
        onClick={e => {
          window.location.assign(
            "https://www.linkedin.com/in/reza-babakhani-634725b1"
          );
        }}
      >
        Reza babakhani
      </NavLink>
      <br />
      GITLAB :{" "}
      <NavLink
        onClick={e => {
          window.location.assign("https://gitlab.com/itsrezababakhani");
        }}
      >
        itsrezababakhani
      </NavLink>
      <br />
      Dribbble :{" "}
      <NavLink
        onClick={e => {
          window.location.assign("https://dribbble.com/itsreza");
        }}
      >
        itsreza
      </NavLink>
      <br />
      Personal Website : itsreza.com
    </div>
  );
}
