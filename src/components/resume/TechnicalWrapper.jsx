import React from "react";
import ProgressBar from "./ProgressBar";
export default function TechnicalWrapper() {
  return (
    <div className="wrapper">
      <div className="exprience col-lg-6 float-left">
        <label>Skills : </label>

        <ProgressBar title="HTML5" percent="100%" value="100" />
        <ProgressBar title="CSS3" percent="100%" value="100" />
        <ProgressBar title="JavaScript" percent="95%" value="95" />
        <ProgressBar title="React JS" percent="90%" value="90" />
        <ProgressBar title="WebPack" percent="100%" value="100" />
        <ProgressBar title="Node JS" percent="90%" value="90" />
        <ProgressBar title="Design Pattern" percent="70%" value="70" />
        <ProgressBar title="MYSQL &amp; Maria DB" percent="90%" value="90" />
        <ProgressBar title="Sketch" percent="100%" value="100" />
        <ProgressBar title="PhotoShop" percent="100%" value="100" />
        <ProgressBar title="AdobeXD" percent="100%" value="100" />
        <ProgressBar title="illustrator" percent="80%" value="80" />
        <ProgressBar title="Ui/Ux" percent="100%" value="100" />
      </div>
      <div className="col-lg-6 pad_part float-left">
        <label className="label_wrapper">Education :</label>
        <p className="p_education">
          Azad Tehran-North University since September 2014 from ... (to be
          Continued)
        </p>
        <label className="label_wrapper">Additional Knowledge :</label>
        <ul className="know_list">
          <li className="knowledge_tag">
            <label>Git</label>
          </li>
          <li className="knowledge_tag">
            <label>Laravel Blades</label>
          </li>

          <li className="knowledge_tag">
            <label>Design System</label>
          </li>
          <li className="knowledge_tag">
            <label>Portotype</label>
          </li>
          <li className="knowledge_tag">
            <label>Material Design</label>
          </li>
          <li className="knowledge_tag">
            <label>HIG IOS</label>
          </li>
          <li className="knowledge_tag">
            <label>More Item Can be Here ...</label>
          </li>
        </ul>
        <div className="clearfix"></div>
        <label className="label_wrapper">Work Experience :</label>
        <div className="work_box">
          <img
            className="float-left logo_company"
            src="assets/images/idea.jpeg"
            alt="alt"
          />
          <div className="float-left work_flow">
            <h3>
              Front-end Developer at <span>ایده پردازان</span>
            </h3>
            <label>for a year</label>
          </div>
        </div>
        <div className="clearfix"></div>
        <div className="work_box">
          <img
            className="float-left logo_company"
            src="assets/images/asia.png"
            alt="alt"
          />
          <div className="float-left work_flow">
            <h3>
              Front-end Developer at <span>ایده پردازان</span>
            </h3>
            <label>for 6 mounth</label>
          </div>
        </div>
        <div className="clearfix"></div>
        <div className="work_box">
          <img
            className="float-left logo_company"
            src="assets/images/freelance.png"
            alt="alt"
          />
          <div className="float-left work_flow">
            <h3>Freelancer Develope</h3>
            <label>for 5 mounth</label>
          </div>
        </div>
        <div className="clearfix"></div>
        <labe className="label_wrapper">Project Experience :</labe>

        <label className="project_info proj_a">
          :: FrontEnd &amp; UI/UX Developer at Servisyab.com
        </label>
        <label className="project_info">
          :: FrontEnd &amp; UI/UX Developer at Rahdoni.com
        </label>
        <label className="project_info">
          :: Amirkabir-isi.ir Wordpress at freelance
        </label>
        <label className="project_info">
          :: lavasani-olive.com Wordpress at freelance
        </label>
        <label className="project_info">
          :: kamalishop.com Wordpress at freelance
        </label>
      </div>
      <div className="clearfix"></div>
    </div>
  );
}
