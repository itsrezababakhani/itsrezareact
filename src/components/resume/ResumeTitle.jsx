import React from "react";
export default function ResumeTitle(props) {
  return <div className="resume_title">{props.title}</div>;
}
