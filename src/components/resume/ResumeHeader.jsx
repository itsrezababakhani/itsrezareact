import React from "react";
function ResumeHeader() {
  return (
    <div>
      <div className="container_resume container-fluid">
        <div className="row">
          <div className="col-lg-12 header_area">
            <div className="header_resume">
              <div className="small_info">
                <div className="blur_info_area">
                  <div className="contact_info_social">
                    <img
                      className="prof_img float-right"
                      src="assets/images/resume.jpg"
                      alt=""
                    />
                    <div className="float-right info_desc">
                      <h1>رضا باباخانی</h1>
                      <h2>توسعه دهنده ی نرم افزار</h2>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
export default ResumeHeader;
