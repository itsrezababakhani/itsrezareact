import React from "react";
export default function SliderItem({ items }) {
  return (
    <div class="main_slider_item">
      <div class="col-lg-12 slider_box">
        <div class="col-lg-12 slider_header">
          <p>{items.description}</p>
        </div>
        <a href="#">
          <div class="brand_logo_area">
            <img
              class="brand_logo_img float-right"
              src={`assets/images/${items.image}`}
              alt=""
            />
            <div class="float-right logo_sub">
              <h2 class="brand_name">{items.title}</h2>
              <h2 class="category_brand">{items.sub_category}</h2>
            </div>
            <div class="button_direct float-left">
              <span class="redirect">ورود به {items.title}</span>
            </div>
          </div>
        </a>
      </div>
    </div>
  );
}
