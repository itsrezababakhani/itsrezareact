import React, { useState, useEffect } from "react";
import SliderItem from "./SliderItem";
import HttpHandler from "../../HttpHandler";
export default function BrandsSlider() {
  let [brands, setBrands] = useState([]);
  const http = new HttpHandler();
  useEffect(() => {
    http.get("/api/v1/brands/get").then(result => {
      setBrands(result.data.brands);
    });
  }, brands.length);
  const sliderItems = brands.map(item => {
    return <SliderItem items={item} />;
  });
  return (
    <div class="row relative_row">
      <span class="span_right_transparent"></span>
      <span class="span_left_transparent"></span>
      <div class="owl-carousel" id="slider1">
        {sliderItems}
      </div>
    </div>
  );
}
