import React from "react";
export default function Projection() {
  return (
    <div class="row section_special">
      <div class="col-lg-6 column_6">
        <div class="content_scope">
          <h2 class="title_scope">متفکرین برتر</h2>
          <h3 class="span_scope">
            خلاقیت از شما دانش و پیاده سازی ایده ها از ما
          </h3>
          <span class="lets_go_span">
            برای رسیدن به رویاهات تا دیر نشده شروع کن
          </span>
          <div class="button_start">
            <a href="#" class="start_pro">
              درخواست پروژه
            </a>
          </div>
        </div>
      </div>

      <div class="col-lg-6">
        <img class="puzzle_img" src="assets/images/puzzle.svg" alt="" />
      </div>
    </div>
  );
}
