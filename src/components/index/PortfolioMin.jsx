import React, { useState, useEffect } from "react";
import PortfolioShow from "../partials/PortfolioShow";
import HttpHandler from "../../HttpHandler";
import Categories from "../../utility/Categories";
import Types from "../../utility/Types";
import Spliter from "../partials/Spliter";

export default function PortfolioMin(props) {
  const http = new HttpHandler();
  let [item, setItem] = useState([]);

  useEffect(() => {
    const getPortfolioTrends = () => {
      http.get("/api/v1/portfolio/trends").then(result => {
        return setItem(result.data.posts);
      });
    };
    getPortfolioTrends();
  }, item.length);

  const portfolioItems = item.map(portfolio => {
    portfolio.category = Categories(portfolio.category);
    portfolio.type = Types(portfolio.type);
    return <PortfolioShow key={portfolio.id} items={portfolio} />;
  });

  return (
    <div className="container portfolio_container">
      <div className="row">
        <Spliter title="بروز ترین نمونه کار ها" />
      </div>
      <hr className="sub_line" />

      <div className="row">{portfolioItems}</div>
    </div>
  );
}
