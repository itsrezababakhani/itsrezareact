import React, { useState, useEffect } from "react";
import HttpHandler from "../../HttpHandler";
import CourseShow from "../partials/CourseShow";
import Loading from "../loading/Loading";
const http = new HttpHandler();
export default function CourseMin() {
  const [item, setItem] = useState([]);
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    http
      .get("/api/v1/tutorial/trends")
      .then(result => {
        setItem(result.data.tutorials);
      })
      .catch(err => err)
      .finally(() => {
        setLoading(false);
      });
  }, item.length);

  const tutorialPost = item.map(item => {
    return loading ? (
      <Loading />
    ) : (
      <CourseShow key={item.id} items={item} loader={loading} />
    );
  });

  return <div className="CourseMin">{tutorialPost}</div>;
}
