import React from "react";

export default function Introduce() {
  return (
    <div className="row">
      <div className="main_content">
        <div className="float-right">
          <div className="logo_img">
            <img src="assets/images/itsreza_logo.svg" alt="itsreza" />
          </div>
          <div className="col social_icon">
            <ul className="col social_list">
              <li className="social_item">
                <a href="#">
                  <i className="icons icon-telegram"></i>
                </a>
              </li>
              <li className="social_item">
                <a href="#">
                  <i className="icons icon-instagram"></i>
                </a>
              </li>
              <li className="social_item">
                <a href="#">
                  <i className="icons icon-dribbble"></i>
                </a>
              </li>
              <li className="social_item">
                <a href="#">
                  <i className="icons icon-github-logo"></i>
                </a>
              </li>
            </ul>
          </div>
          <div className="clearfix"></div>
          <div className="col job_position">
            <h1>web developer</h1>
            <h1 className="float-left">UI & UX </h1>
            <span className="sub_position">Pro Designer</span>
          </div>
        </div>
        <div className="float-right">
          <img src="assets/images/monster.svg" alt="monster" />
        </div>

        <span className="monster_description">
          آماده برای ساخت رویا های شما<i></i>
        </span>
      </div>
    </div>
  );
}
