import React from "react";
import notFound from "./404.png";
import { Link } from "react-router-dom";
export default function NotFoundError() {
  return (
    <div className="container-fluid error_container">
      <img src={notFound} alt="notFound" />
      <p className="oops">اوه .. !!</p>
      <p className="oops_description">صفحه ی درخواستی شما موجود نمی باشد .</p>
      <Link className="back_home" to="/">
        بازگشت به صفحه ی اصلی
      </Link>
    </div>
  );
}
