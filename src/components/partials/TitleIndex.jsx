import React from "react";
export default function TitleIndex(props) {
  return (
    <div className="container-fluid">
      <div className="row title_section">
        <div className="title_area">
          <img src="../assets/images/titlebg.svg" alt="title" />
          <h2 className="eng_title">{props.en_title}</h2>
          <h2 className="fa_title">{props.fa_title}</h2>
        </div>
      </div>
    </div>
  );
}
