import React from "react";
import { Link } from "react-router-dom";
export default function PortfolioShow({ items }) {
  return (
    <div class="col-lg-4">
      <Link to={`portfolio/${items.id}`} class="box_link hvr-grow">
        <div class="box">
          <div class="box_content">
            <div class="box_img">
              <img
                class="post_img"
                src={`assets/images/${items.image}`}
                alt=""
              />
            </div>
            <div class="box_title">
              <h2>{items.title}</h2>
            </div>
          </div>
          <img
            class="category_icon_post"
            src={`../assets/images/${items.category}.svg`}
            alt="test"
          />

          {items.type === "file" ? (
            <i class="icon_type_post icons icon-attachment"></i>
          ) : null}
          {items.type === "article" ? (
            <i class="icon_type_post icons icon-document"></i>
          ) : null}
          {items.type === "video" ? (
            <i class="icon_type_post icons icon-videoplayer"></i>
          ) : null}
        </div>
      </Link>
    </div>
  );
}
