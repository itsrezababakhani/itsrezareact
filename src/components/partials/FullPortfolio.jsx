import React from "react";
export default function FullPortfolio(props) {
  return (
    <div className="col-lg-4 float-right">
      <div className="box">
        <div className="box_content">
          <div className="box_img">
            <img
              class="min_post_img"
              src={`assets/images/${props.image}`}
              alt=""
            />
          </div>
          <div class="min_box_title">
            <h2>{props.title}</h2>
          </div>
        </div>
      </div>
    </div>
  );
}
