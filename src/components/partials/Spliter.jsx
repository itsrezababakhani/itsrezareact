import React from "react";
export default function Spliter(props) {
  return (
    <div className="col-lg-12">
      <h2 className="col-lg-10 float-right sub_fa_title">{props.title}</h2>
      <div className="col-lg-2 float-left">
        <a className="more_archive" href="#">
          بیشتر
        </a>
      </div>
      <div className="clearfix"></div>
    </div>
  );
}
