import React from "react";
import { NavLink } from "react-router-dom";
import CategoryByName from "../../utility/CategoryByName";
import Types from "../../utility/Types";
import LazyLoad from "react-lazyload";
export default function CourseShow({ items }) {
  return (
    <div className="col-lg-4 learn_box">
      <div className="learn_inner">
        <div className="learn_header">
          <NavLink to={`/course/${items.id}`} className="link_inner">
            <LazyLoad
              height={200}
              offset={100}
              alt="loader"
              src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/17/Yin_yang.svg/1024px-Yin_yang.svg.png"
            >
              <img
                src={`assets/images/${items.image}`}
                className="learn_img"
                alt={items.title}
              />
            </LazyLoad>
          </NavLink>
        </div>
        <div className="learn_info">
          <div className="learn_content text-right">
            <NavLink to={`/course/${items.id}`} className="learn_title">
              {items.title}
            </NavLink>

            <div className="clearfix learn_description">
              {items.description}
            </div>
          </div>

          <div className="item_info row">
            <div className="level_inner col-md-3 hidden-xs hidden-sm category_box">
              {Types(items.type) === "file" ? (
                <i className="icons icon-attachment"></i>
              ) : (
                ""
              )}
              {Types(items.type) === "video" ? (
                <i class="icons icon-videoplayer"></i>
              ) : (
                ""
              )}
              {Types(items.type) === "article" ? (
                <i class="icons icon-document"></i>
              ) : (
                ""
              )}
            </div>
            <div className="level_inner off_percent col-md-4 col-xs-6">
              <span className="off_percent">دسته بندی</span>
              {CategoryByName(items.category)}
            </div>
            <div className="level_inner price_ticket col-md-5 col-xs-6">
              <span>قیمت</span>
              {items.price} تومان
            </div>
          </div>
        </div>
      </div>
      <img className="category_icon_post" src="images/adobexd.svg" alt="" />
    </div>
  );
}
