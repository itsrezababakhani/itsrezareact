import React, { useEffect, useState } from "react";
import HttpHandler from "../../HttpHandler";
import FullPortfolio from "../partials/FullPortfolio";
export default function PortfolioShow() {
  const [posts, setPosts] = useState([]);
  const http = new HttpHandler();
  useEffect(() => {
    http
      .get("/api/v1/portfolio")
      .then(response => {
        setPosts(response.data.posts);
      })
      .catch(err => {
        return err;
      });
  }, [posts.length * 2]);
  const renderItems = posts.map(post => {
    return <FullPortfolio {...post} />;
  });
  return <div>{renderItems}</div>;
}
