import React from "react";

export default function Loading() {
  return (
    <div>
      <div class="spinner-grow text-info" role="status">
        <span class="sr-only">Loading...</span>
      </div>
    </div>
  );
}
