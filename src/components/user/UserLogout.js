import React from "react";
import { Redirect } from "react-router-dom";

export default function UserLogout() {
  return (
    <div>
      {localStorage.removeItem("token")}
      {localStorage.removeItem("username")}
      <Redirect to="/" />
    </div>
  );
}
